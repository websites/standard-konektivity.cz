const webpack = require("webpack");
const WebpackDevServer = require("webpack-dev-server");
const config = require("./webpack.config");

new WebpackDevServer(webpack(config), {
    publicPath: config.output.publicPath,
    quiet: false,
    noInfo: false,
    hot: true,
    stats: {
        assets: true,
        colors: true,
        version: false,
        hash: false,
        timings: false,
        chunks: false,
        chunkModules: false
    },
    headers: {
        "Access-Control-Allow-Origin": "*"
    },
    inline: true,
    historyApiFallback: true
}).listen(8001, "0.0.0.0", function (err, result) {
    if (err) {
        console.log(err);
    }

    console.log("Listening at 0.0.0.0:8001");
});
