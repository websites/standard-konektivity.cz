from rest_framework import viewsets
from .models import ConnectionTestResult, WebsiteTestResult, speed_diff
from .serializers import ConnectionTestResultSerializer, WebsiteTestResultSerializer
from django.http import HttpResponse
from django.core.validators import validate_ipv46_address
from django.core.exceptions import ValidationError
from django.conf import settings
from django.utils.ipv6 import is_valid_ipv6_address
from django.core.cache import caches
from urllib.parse import urlparse
import json
import requests
import re
import dns.resolver
import dns.message
import dns.query
import tldextract

resolver = dns.resolver.Resolver(configure=False)
resolver.nameservers = settings.NAMESERVERS_DNSSEC
resolver.timeout = 15
resolver.lifetime = 15


def get_record(domain, record):
    domain = dns.name.from_text(domain)
    try:
        r = resolver.query(domain, record, dns.rdataclass.IN)
        rcode = r.response.rcode()
        if rcode == 2:  # SERVFAIL
            return False
    except (dns.resolver.NoAnswer,
            dns.resolver.NXDOMAIN,
            dns.resolver.Timeout,
            dns.resolver.NoNameservers):
        return False
    else:
        return True


def check_dnssec(domain):
    domain = dns.name.from_text(domain)
    q = dns.message.make_query(domain, dns.rdatatype.A, dns.rdataclass.IN, want_dnssec=True)
    r = dns.query.tls(q, settings.NAMESERVERS_DNSSEC[0], timeout=15)
    if len(r.answer) < 1:
        return False
    flags = dns.flags.to_text(r.flags).split(" ")
    return "AD" in flags
    

class ConnectionTestResultList(viewsets.ModelViewSet):
    queryset = ConnectionTestResult.objects.all()
    serializer_class = ConnectionTestResultSerializer


class WebsiteTestResultList(viewsets.ModelViewSet):
    queryset = WebsiteTestResult.objects.all()
    serializer_class = WebsiteTestResultSerializer


def calculate_speed_diff(request):
    try:
        down = float(request.GET.get("down"))
        up = float(request.GET.get("up"))
    except (ValueError, TypeError):
        data = {
            "error": "Both `down` and `up` need to be numeric values."
        }
        status_code = 400
    else:
        data = {
            "down": down,
            "up": up,
            "diff": speed_diff(down, up)
        }
        status_code = 200
    return HttpResponse(json.dumps(data),
                        content_type="application/json",
                        status=status_code)


def eduroam(request, try_subdomains=False):
    url = request.GET.get("url", "")
    if len(url) < 1:
        return HttpResponse(json.dumps({"error": "Invalid URL."}),
                            content_type="application/json",
                            status=400)
    domain = re.sub(r"^https?://", "", url)
    domain = re.sub(r"^www.", "", domain)
    if try_subdomains:
        domain = "eduroam." + domain
    cache = caches["default"]
    r = cache.get("eduroam_json")
    if r is None:
        r = requests.get(settings.EDUROAM_JSON, timeout=5).json()
        cache.set("eduroam_json", r, 60 * 1)  # 1 minute
    matching_orgs = list(filter(
        lambda org: list(filter(
            lambda realm: realm == domain,
            org["realms"])),
        r))
    if len(matching_orgs) > 0:
        data = {"eduroam": True, "orgs": matching_orgs}
    else:
        if try_subdomains:
            data = {"eduroam": False}
        else:
            return eduroam(request, try_subdomains=True)
    return HttpResponse(json.dumps(data), content_type="application/json")


def get_https_status(domain):
    try:
        r = requests.get("https://" + domain, timeout=15, headers={"User-Agent": ""})
        status = r.ok or 400 < r.status_code < 500
    except (requests.exceptions.Timeout,
            requests.exceptions.ConnectionError,
            requests.exceptions.HTTPError,
            requests.exceptions.TooManyRedirects,
            requests.exceptions.SSLError):
        status = False
    return status


def website_status(request):
    url = request.GET.get("url", "")
    if not re.match(r"^(https?)://.*", url):
        url = "http://" + url
    parsed_uri = urlparse(url)
    domain = "{uri.netloc}".format(uri=parsed_uri)
    domain = re.sub(r"^www.", "", domain)
    ext = tldextract.extract(url)
    if not ext.suffix or not ext.domain:
        return HttpResponse(json.dumps({"error": "Invalid URL."}),
                            content_type="application/json",
                            status=400)

    data = {
        "domain": domain
    }

    data["https"] = get_https_status(domain) or get_https_status("www." + domain)
    data["ipv4"] = get_record(domain, "A")
    data["ipv6"] = get_record(domain, "AAAA")
    data["dnssec"] = check_dnssec(domain)

    return HttpResponse(json.dumps(data), content_type="application/json")


def ripe_ip_info(request):
    def get_attr(json, name):
        for field in json:
            if field["name"] == name:
                return field["value"]
        return False

    def get_inetnum(json):
        inetnum = get_attr(json, "inetnum")
        if inetnum is False:
            inetnum = get_attr(json, "inet6num")
        return inetnum

    try:
        ip = request.GET.get("ip")
        validate_ipv46_address(ip)
    except (ValidationError, TypeError):
        data = {
            "error": "Invalid IP address."
        }
        status_code = 400
    else:
        if is_valid_ipv6_address(ip):
            protocol = "ipv6"
            ripe_url = "https://rest.db.ripe.net/search?type-filter=inet6num"
        else:
            protocol = "ipv4"
            ripe_url = "https://rest.db.ripe.net/search?type-filter=inetnum"
        ripe_url = ripe_url + "&source=ripe&query-string="
        r = requests.get(ripe_url + ip, headers={"Accept": "application/json"}, timeout=5)
        ripe_json = r.json()["objects"]["object"][0]["attributes"]["attribute"]
        data = {
            "ip": ip,
            "protocol": protocol,
            "netname": get_attr(ripe_json, "netname"),
            "inetnum": get_inetnum(ripe_json),
            "link": r.json()["objects"]["object"][0]["link"]["href"]
        }
        # data = r.json()
        status_code = 200
    return HttpResponse(json.dumps(data),
                        content_type="application/json",
                        status=status_code)
