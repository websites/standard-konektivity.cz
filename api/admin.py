from django.contrib import admin
from .models import ConnectionTestResult, WebsiteTestResult
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _


def format_speed(speed):
    if speed:
        return str(round(speed, 3)) + " Mb/s"


def format_ms(ms):
    if ms:
        return str(round(ms, 2)) + " ms"


def format_ratio(ratio):
    if ratio:
        display = str(round(ratio * 100, 2)) + " %"
        color = "#a1c454"
        if ratio > 0.3:
            color = "#c45454"
        return format_html("<span style='color:%s;'>%s</span>" % (color, display))


def netmetr_link(uuid):
    if uuid:
        return format_html("<a href='%s%s' target='_blank'>Odkaz</a>"
                           % ("https://www.netmetr.cz/cs/detail.html?", uuid))


def ripe_link(netname, link):
    if link and netname:
        return format_html("<a href='%s' target='_blank'>%s</a>"
                           % (link, netname))


class ConnectionTestResultAdmin(admin.ModelAdmin):
    list_per_page = 15

    def netmetr4_field(self, obj):
        return netmetr_link(obj.netmetr_uuid_ipv4)

    netmetr4_field.short_description = _("NetMetr IPv4")

    def netmetr6_field(self, obj):
        return netmetr_link(obj.netmetr_uuid_ipv6)

    netmetr6_field.short_description = _("NetMetr IPv6")

    def ripe4_field(self, obj):
        return ripe_link(obj.ripe_netname_ipv4, obj.ripe_link_ipv4)

    ripe4_field.short_description = _("IPv4 netname")

    def ripe6_field(self, obj):
        return ripe_link(obj.ripe_netname_ipv6, obj.ripe_link_ipv6)

    ripe6_field.short_description = _("IPv6 netname")

    def speed_ipv4_down_field(self, obj):
        return format_speed(obj.speed_ipv4_down)

    speed_ipv4_down_field.short_description = _("IPv4 down")
    speed_ipv4_down_field.admin_order_field = "speed_ipv4_down"

    def speed_ipv4_up_field(self, obj):
        return format_speed(obj.speed_ipv4_up)

    speed_ipv4_up_field.short_description = _("IPv4 up")
    speed_ipv4_up_field.admin_order_field = "speed_ipv4_up"

    def speed_ipv6_down_field(self, obj):
        return format_speed(obj.speed_ipv6_down)

    speed_ipv6_down_field.short_description = _("IPv6 down")
    speed_ipv6_down_field.admin_order_field = "speed_ipv6_down"

    def speed_ipv6_up_field(self, obj):
        return format_speed(obj.speed_ipv6_up)

    speed_ipv6_up_field.short_description = _("IPv6 up")
    speed_ipv6_up_field.admin_order_field = "speed_ipv6_up"

    def ping_ipv4_field(self, obj):
        return format_ms(obj.ping_ipv4)

    ping_ipv4_field.short_description = _("IPv4 ping")
    ping_ipv4_field.admin_order_field = "ping_ipv4"

    def ping_ipv6_field(self, obj):
        return format_ms(obj.ping_ipv6)

    ping_ipv6_field.short_description = _("IPv6 ping")
    ping_ipv6_field.admin_order_field = "ping_ipv6"

    def speed_ipv4_ratio_field(self, obj):
        return format_ratio(obj.speed_ipv4_ratio)

    speed_ipv4_ratio_field.short_description = _("IPv4 diff")
    speed_ipv4_ratio_field.admin_order_field = "speed_ipv4_ratio"

    def speed_ipv6_ratio_field(self, obj):
        return format_ratio(obj.speed_ipv6_ratio)

    speed_ipv6_ratio_field.short_description = _("IPv6 diff")
    speed_ipv6_ratio_field.admin_order_field = "speed_ipv6_ratio"

    def download_ratio_field(self, obj):
        return format_ratio(obj.download_ratio)

    download_ratio_field.short_description = _("Download diff")
    download_ratio_field.admin_order_field = "download_ratio"

    def upload_ratio_field(self, obj):
        return format_ratio(obj.upload_ratio)

    upload_ratio_field.short_description = _("Upload diff")
    upload_ratio_field.admin_order_field = "upload_ratio"

    list_display = [
        "timestamp",
        "has_ipv4",
        "user_ipv4",
        "ripe4_field",
        "has_ipv6",
        "user_ipv6",
        "ripe6_field",
        "has_dnssec_rsa",
        "has_dnssec_ecdsa",
        "has_fenix",
        "has_eduroam",
        "speed_ipv4_down_field",
        "speed_ipv4_up_field",
        "ping_ipv4_field",
        "speed_ipv4_ratio_field",
        "netmetr4_field",
        "speed_ipv6_down_field",
        "speed_ipv6_up_field",
        "ping_ipv6_field",
        "speed_ipv6_ratio_field",
        "upload_ratio_field",
        "netmetr6_field",
        "download_ratio_field"
    ]
    list_filter = (
        "timestamp",
        "has_ipv4",
        "has_ipv6",
        "has_dnssec_rsa",
        "has_dnssec_ecdsa",
        "has_fenix",
        "has_eduroam"
    )
    search_fields = [
        "ripe_netname_ipv4",
        "ripe_netname_ipv6",
        "user_ipv4",
        "user_ipv6"
    ]
    ordering = ["-timestamp"]


class WebsiteTestResultAdmin(admin.ModelAdmin):
    list_per_page = 15

    list_display = [
        "timestamp",
        "domain",
        "has_ipv4",
        "has_ipv6",
        "has_dnssec",
        "has_https"
    ]
    list_filter = (
        "timestamp",
        "has_ipv4",
        "has_ipv6",
        "has_dnssec",
        "has_https",
        "domain",
    )
    search_fields = [
        "domain"
    ]
    ordering = ["-timestamp"]


admin.site.register(ConnectionTestResult, ConnectionTestResultAdmin)
admin.site.register(WebsiteTestResult, WebsiteTestResultAdmin)
