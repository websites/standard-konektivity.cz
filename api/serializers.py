from rest_framework import serializers

from .models import ConnectionTestResult, WebsiteTestResult


class ConnectionTestResultSerializer(serializers.ModelSerializer):

    class Meta:
        model = ConnectionTestResult
        fields = (
            "id",
            "has_dnssec_ecdsa",
            "has_dnssec_rsa",
            "has_fenix",
            "has_eduroam",
            "has_ipv4",
            "has_ipv6",
            "netmetr_uuid_ipv4",
            "netmetr_uuid_ipv6",
            "ping_ipv4",
            "ping_ipv6",
            "ripe_link_ipv4",
            "ripe_link_ipv6",
            "ripe_netname_ipv4",
            "ripe_netname_ipv6",
            "speed_ipv4_down",
            "speed_ipv4_ratio",
            "speed_ipv4_up",
            "speed_ipv6_down",
            "speed_ipv6_ratio",
            "speed_ipv6_up",
            "timestamp",
            "user_ipv4",
            "user_ipv6"
        )


class WebsiteTestResultSerializer(serializers.ModelSerializer):

    class Meta:
        model = WebsiteTestResult
        fields = (
            "id",
            "timestamp",
            "domain",
            "has_ipv4",
            "has_ipv6",
            "has_dnssec",
            "has_https"
        )
