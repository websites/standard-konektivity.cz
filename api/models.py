from django.db import models
from django.core import validators
from django.utils.translation import ugettext_lazy as _
from django.dispatch import receiver
import uuid

netmetr_uuid_regex = "^O?[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$"
timestamp_display_format = "%Y-%m-%d %H:%M:%S"
uuid_validators = [
    validators.RegexValidator(netmetr_uuid_regex)
]


def speed_diff(down, up):
    speed1 = min(down, up)
    speed2 = max(down, up)
    if speed1 == 0 and speed2 == 0:
        return 0
    if speed2 == 0:
        return 1
    else:
        return round(abs(-1 + speed1 / speed2), 4)


class ConnectionTestResult(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user_ipv4 = models.GenericIPAddressField(_("IPv4 address"), protocol="ipv4", null=True,
                                             blank=True)
    ripe_netname_ipv4 = models.CharField(_("IPv4 RIPE netname"), max_length=255,
                                         blank=True, null=True)
    ripe_link_ipv4 = models.CharField(_("IPv4 RIPE inetnum link"), max_length=255,
                                      blank=True, null=True)
    user_ipv6 = models.GenericIPAddressField(_("IPv6 address"), protocol="ipv6", null=True,
                                             blank=True)
    ripe_netname_ipv6 = models.CharField(_("IPv6 RIPE netname"), max_length=255,
                                         blank=True, null=True)
    ripe_link_ipv6 = models.CharField(_("IPv6 RIPE inetnum link"), max_length=255,
                                      blank=True, null=True)
    speed_ipv4_down = models.FloatField(_("IPv4 down"), null=True, blank=True)
    speed_ipv4_up = models.FloatField(_("IPv4 up"), null=True, blank=True)
    speed_ipv4_ratio = models.FloatField(
        _("IPv4 ratio"), null=True, editable=False, blank=True)
    speed_ipv6_down = models.FloatField(_("IPv6 down"), null=True, blank=True)
    speed_ipv6_up = models.FloatField(_("IPv6 up"), null=True, blank=True)
    speed_ipv6_ratio = models.FloatField(
        _("IPv6 ratio"), null=True, editable=False, blank=True)
    download_ratio = models.FloatField(
        _("Download ratio"), null=True, editable=False, blank=True)
    upload_ratio = models.FloatField(
        _("Upload ratio"), null=True, editable=False, blank=True)
    ping_ipv4 = models.FloatField(_("IPv4 ping"), null=True, blank=True)
    ping_ipv6 = models.FloatField(_("IPv6 ping"), null=True, blank=True)
    netmetr_uuid_ipv4 = models.CharField(_("NetMetr IPv4"), max_length=40, blank=True, null=True,
                                         unique=True, validators=uuid_validators)
    netmetr_uuid_ipv6 = models.CharField(_("NetMetr IPv6"), max_length=40, blank=True, null=True,
                                         unique=True, validators=uuid_validators)
    has_ipv4 = models.BooleanField(_("IPv4"))
    has_ipv6 = models.BooleanField(_("IPv6"))
    has_dnssec_rsa = models.BooleanField(_("DNSSEC RSA"))
    has_dnssec_ecdsa = models.BooleanField(_("DNSSEC ECDSA"))
    has_fenix = models.BooleanField(_("FENIX"))
    has_eduroam = models.NullBooleanField(_("Eduroam"), blank=True, null=True)
    timestamp = models.DateTimeField(
        _("Time"), auto_now_add=True, editable=False)

    def __str__(self):
        return self.timestamp.strftime(timestamp_display_format)

    class Meta:
        verbose_name = _("Connection test")
        verbose_name_plural = _("Connection tests")
        ordering = ["-timestamp"]


@receiver(models.signals.pre_save, sender=ConnectionTestResult)
def before_connectiontestresult_save(sender, instance, **kwargs):
    if instance.speed_ipv4_down and instance.speed_ipv4_up:
        instance.speed_ipv4_ratio = speed_diff(
            instance.speed_ipv4_down, instance.speed_ipv4_up)
    if instance.speed_ipv6_down and instance.speed_ipv6_up:
        instance.speed_ipv6_ratio = speed_diff(
            instance.speed_ipv6_down, instance.speed_ipv6_up)
    if instance.speed_ipv4_down and instance.speed_ipv6_down:
        instance.download_ratio = speed_diff(
            instance.speed_ipv4_down, instance.speed_ipv6_down)
    if instance.speed_ipv4_up and instance.speed_ipv6_up:
        instance.upload_ratio = speed_diff(
            instance.speed_ipv4_up, instance.speed_ipv6_up)
    instance.full_clean()


class WebsiteTestResult(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    domain = models.CharField(_("Domain"), max_length=128)
    has_ipv4 = models.NullBooleanField(_("IPv4"))
    has_ipv6 = models.NullBooleanField(_("IPv6"))
    has_dnssec = models.NullBooleanField(_("DNSSEC"))
    has_https = models.NullBooleanField(_("HTTPS"))
    timestamp = models.DateTimeField(
        _("Time"), auto_now_add=True, editable=False)

    def __str__(self):
        return self.timestamp.strftime(timestamp_display_format) + " – " + self.domain

    class Meta:
        verbose_name = _("Website test")
        verbose_name_plural = _("Website tests")
        ordering = ["-timestamp"]
