# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-02 10:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0003_auto_20170512_1644'),
    ]

    operations = [
        migrations.AddField(
            model_name='connectiontestresult',
            name='download_ratio',
            field=models.FloatField(blank=True, editable=False, null=True, verbose_name='Download ratio'),
        ),
        migrations.AddField(
            model_name='connectiontestresult',
            name='upload_ratio',
            field=models.FloatField(blank=True, editable=False, null=True, verbose_name='Upload ratio'),
        ),
        migrations.AlterField(
            model_name='connectiontestresult',
            name='ripe_link_ipv4',
            field=models.CharField(blank=True, max_length=255, verbose_name='IPv4 RIPE inetnum link'),
        ),
        migrations.AlterField(
            model_name='connectiontestresult',
            name='ripe_link_ipv6',
            field=models.CharField(blank=True, max_length=255, verbose_name='IPv6 RIPE inetnum link'),
        ),
    ]
