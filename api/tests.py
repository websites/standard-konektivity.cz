from django.test import TestCase, RequestFactory
from .views import calculate_speed_diff, website_status, ripe_ip_info
import json


# class DnsTestCase(TestCase):
#     def setUp(self):
#         settings.NUM_LATEST = 5


class SpeedDiffTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_return_code(self):
        """Call to speed_diff returns HTTP 200 when given umeric values."""
        request = self.factory.get("/api/speed_diff", {"up": 1, "down": 1})
        response = calculate_speed_diff(request)
        self.assertEqual(response.status_code, 200)

    def test_commutative(self):
        """Call to speed_diff returns the same result when down and up values are swapped."""
        request1 = self.factory.get("/api/speed_diff", {"up": 50, "down": 25})
        response1 = calculate_speed_diff(request1)
        request2 = self.factory.get("/api/speed_diff", {"up": 25, "down": 50})
        response2 = calculate_speed_diff(request2)
        self.assertEqual(response1.status_code, 200)
        self.assertEqual(response2.status_code, 200)
        self.assertEqual(json.loads(response1.content.decode("utf-8"))["diff"],
                         json.loads(response2.content.decode("utf-8"))["diff"])

    def test_float(self):
        """Call to speed_diff returns correct result when given float values."""
        request = self.factory.get("/api/speed_diff", {"up": 54.4, "down": 27.2})
        response = calculate_speed_diff(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode("utf-8")),
                         {"down": 27.2, "up": 54.4, "diff": 0.5})

    def test_zero_values(self):
        """Call to speed_diff handles zero values correctly."""
        request1 = self.factory.get("/api/speed_diff", {"up": 0, "down": 0})
        response1 = calculate_speed_diff(request1)
        request2 = self.factory.get("/api/speed_diff", {"up": 1, "down": 0})
        response2 = calculate_speed_diff(request2)
        request3 = self.factory.get("/api/speed_diff", {"up": 0, "down": 1})
        response3 = calculate_speed_diff(request3)
        self.assertEqual(response1.status_code, 200)
        self.assertEqual(response2.status_code, 200)
        self.assertEqual(response3.status_code, 200)
        self.assertEqual(json.loads(response1.content.decode("utf-8")),
                         {"down": 0.0, "up": 0.0, "diff": 0})
        self.assertEqual(json.loads(response2.content.decode("utf-8")),
                         {"down": 0.0, "up": 1.0, "diff": 1.0})
        self.assertEqual(json.loads(response3.content.decode("utf-8")),
                         {"down": 1.0, "up": 0.0, "diff": 1.0})

    def test_number_validation(self):
        """Call to speed_diff returns an error when given non-numeric value."""
        request = self.factory.get("/api/speed_diff", {"up": "foo", "down": "bar"})
        response = calculate_speed_diff(request)
        self.assertEqual(response.status_code, 400)
        self.assertEqual("error" in json.loads(response.content.decode("utf-8")).keys(), True)


class WebsiteStatusTest(TestCase):

    def setUp(self):
        self.factory = RequestFactory()

    def test_nic_cz(self):
        """Call to website_status returns correct values for nic.cz."""
        request = self.factory.get("/api/website_status", {"url": "nic.cz"})
        response = website_status(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode("utf-8")),
                         {
                         "domain": "nic.cz",
                         "https": True,
                         "ipv4": True,
                         "ipv6": True,
                         "dnssec": True
                         })

    def test_rhybar_cz(self):
        """Call to website_status returns correct values for rhybar.cz."""
        request = self.factory.get("/api/website_status", {"url": "rhybar.cz"})
        response = website_status(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode("utf-8")),
                         {
                         "domain": "rhybar.cz",
                         "https": False,
                         "ipv4": False,
                         "ipv6": False,
                         "dnssec": False
                         })

    def test_ipv4(self):
        """Call to website_status returns correct values for IPv4-only domain."""
        request = self.factory.get("/api/website_status", {"url": "control-4.netmetr.cz"})
        response = website_status(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode("utf-8")),
                         {
                         "domain": "control-4.netmetr.cz",
                         "https": True,
                         "ipv4": True,
                         "ipv6": False,
                         "dnssec": True
                         })

    # def test_ipv6(self):
    #     """Call to website_status returns correct values for IPv6-only domain."""
    #     request = self.factory.get("/api/website_status", {"url": "control-6.netmetr.cz"})
    #     response = website_status(request)
    #     self.assertEqual(response.status_code, 200)
    #     self.assertEqual(json.loads(response.content.decode("utf-8")),
    #                      {
    #                      "domain": "control-6.netmetr.cz",
    #                      "https": True,
    #                      "ipv4": False,
    #                      "ipv6": True,
    #                      "dnssec": True
    #                      })

    def test_broken_dnssec_rsa(self):
        """Call to website_status returns `dnssec: false` for a domain with intentionally
        broken DNSSEC (RSA)."""
        request = self.factory.get("/api/website_status", {"url": "bad-rsa.test-ipv6.nic.cz"})
        response = website_status(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode("utf-8")),
                         {
                         "domain": "bad-rsa.test-ipv6.nic.cz",
                         "https": False,
                         "ipv4": False,
                         "ipv6": False,
                         "dnssec": False
                         })

    def test_broken_dnssec_ecdsa(self):
        """Call to website_status returns `dnssec: false` for a domain with intentionally
        broken DNSSEC (ECDSA)."""
        request = self.factory.get("/api/website_status", {"url": "bad-ecdsa.test-ipv6.nic.cz"})
        response = website_status(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode("utf-8")),
                         {
                         "domain": "bad-ecdsa.test-ipv6.nic.cz",
                         "https": False,
                         "ipv4": False,
                         "ipv6": False,
                         "dnssec": False
                         })

    def test_protocol_independence(self):
        """Call to website_status returns correct values for nic.cz regardless
        of given protocol."""
        request1 = self.factory.get("/api/website_status", {"url": "http://nic.cz"})
        response1 = website_status(request1)
        request2 = self.factory.get("/api/website_status", {"url": "https://nic.cz"})
        response2 = website_status(request2)
        self.assertEqual(response1.status_code, 200)
        self.assertEqual(response2.status_code, 200)
        self.assertEqual(json.loads(response1.content.decode("utf-8")),
                         {
                         "domain": "nic.cz",
                         "https": True,
                         "ipv4": True,
                         "ipv6": True,
                         "dnssec": True
                         })
        self.assertEqual(json.loads(response2.content.decode("utf-8")),
                         {
                         "domain": "nic.cz",
                         "https": True,
                         "ipv4": True,
                         "ipv6": True,
                         "dnssec": True
                         })

    def test_idn_domain(self):
        """Call to website_status works with an IDN (non-ascii) domain."""
        request = self.factory.get("/api/website_status", {"url": "háčkyčárky.cz"})
        response = website_status(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode("utf-8")),
                         {
                         "domain": "háčkyčárky.cz",
                         "https": True,
                         "ipv4": True,
                         "ipv6": True,
                         "dnssec": True
                         })

    def test_url_validation_1(self):
        """Call to website_status returns an error for invalid URL: `foo`."""
        request = self.factory.get("/api/website_status", {"url": "foo"})
        response = website_status(request)
        self.assertEqual(response.status_code, 400)
        self.assertEqual("error" in json.loads(response.content.decode("utf-8")).keys(), True)

    def test_url_validation_2(self):
        """Call to website_status returns an error for invalid URL: `0`."""
        request = self.factory.get("/api/website_status", {"url": "0"})
        response = website_status(request)
        self.assertEqual(response.status_code, 400)
        self.assertEqual("error" in json.loads(response.content.decode("utf-8")).keys(), True)

    def test_url_validation_3(self):
        """Call to website_status returns an error for invalid URL: `.`."""
        request = self.factory.get("/api/website_status", {"url": "."})
        response = website_status(request)
        self.assertEqual(response.status_code, 400)
        self.assertEqual("error" in json.loads(response.content.decode("utf-8")).keys(), True)

    def test_url_validation_4(self):
        """Call to website_status returns an error for invalid URL: `domain.zc`."""
        request = self.factory.get("/api/website_status", {"url": "domain.zc"})
        response = website_status(request)
        self.assertEqual(response.status_code, 400)
        self.assertEqual("error" in json.loads(response.content.decode("utf-8")).keys(), True)


class RipeInfoTest(TestCase):

    def setUp(self):
        self.factory = RequestFactory()

    def test_cz_nic_ipv4(self):
        """Call to ripe_ip_info returns correct values for IPv4 from CZ.NIC's range."""
        request = self.factory.get("/api/ripe_ip_info", {"ip": "217.31.205.50"})
        response = ripe_ip_info(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode("utf-8")),
                         {
                         "ip": "217.31.205.50",
                         "protocol": "ipv4",
                         "netname": "CZ-NIC-I",
                         "inetnum": "217.31.205.0 - 217.31.206.255",
                         "link":
                         "http://rest.db.ripe.net/ripe/inetnum/217.31.205.0 - 217.31.206.255"
                         })

    def test_cz_nic_ipv6(self):
        """Call to ripe_ip_info returns correct values for IPv6 from CZ.NIC's range."""
        request = self.factory.get("/api/ripe_ip_info", {"ip": "2001:1488:0:3::2"})
        response = ripe_ip_info(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode("utf-8")),
                         {
                         "ip": "2001:1488:0:3::2",
                         "protocol": "ipv6",
                         "netname": "CZ-NIC-NET",
                         "inetnum": "2001:1488::/48",
                         "link": "http://rest.db.ripe.net/ripe/inet6num/2001:1488::/48"
                         })

    def test_local_ipv4(self):
        """Call to ripe_ip_info returns correct values for 127.0.0.1."""
        request = self.factory.get("/api/ripe_ip_info", {"ip": "127.0.0.1"})
        response = ripe_ip_info(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode("utf-8")),
                         {
                         "ip": "127.0.0.1",
                         "protocol": "ipv4",
                         "netname": "NON-RIPE-NCC-MANAGED-ADDRESS-BLOCK",
                         "inetnum": "125.62.76.0 - 127.255.255.255",
                         "link":
                             "http://rest.db.ripe.net/ripe/inetnum/125.62.76.0 - 127.255.255.255"
                         })

    def test_local_ipv6(self):
        """Call to ripe_ip_info returns correct values for ::1"""
        request = self.factory.get("/api/ripe_ip_info", {"ip": "::1"})
        response = ripe_ip_info(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content.decode("utf-8")),
                         {
                         "ip": "::1",
                         "protocol": "ipv6",
                         "netname": "IANA-BLK",
                         "inetnum": "::/0",
                         "link": "http://rest.db.ripe.net/ripe/inet6num/::/0"
                         })
