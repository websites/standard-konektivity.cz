from django.urls import path
from rest_framework import routers

from . import views

router = routers.SimpleRouter()

router.register(r"connection", views.ConnectionTestResultList)
router.register(r"website", views.WebsiteTestResultList)

urlpatterns = router.urls + [
    path("speed_diff/", views.calculate_speed_diff),
    path("eduroam/", views.eduroam),
    path("website_status/", views.website_status),
    path("ripe_ip_info/", views.ripe_ip_info),
]
