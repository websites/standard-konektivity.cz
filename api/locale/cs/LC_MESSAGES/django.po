# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-06-02 12:40+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: api/admin.py:44 api/models.py:42
msgid "NetMetr IPv4"
msgstr ""

#: api/admin.py:49 api/models.py:45
msgid "NetMetr IPv6"
msgstr ""

#: api/admin.py:54
msgid "IPv4 netname"
msgstr ""

#: api/admin.py:59
msgid "IPv6 netname"
msgstr ""

#: api/admin.py:64 api/models.py:32
msgid "IPv4 down"
msgstr ""

#: api/admin.py:70 api/models.py:33
msgid "IPv4 up"
msgstr ""

#: api/admin.py:76 api/models.py:35
msgid "IPv6 down"
msgstr ""

#: api/admin.py:82 api/models.py:36
msgid "IPv6 up"
msgstr ""

#: api/admin.py:88 api/models.py:40
msgid "IPv4 ping"
msgstr ""

#: api/admin.py:94 api/models.py:41
msgid "IPv6 ping"
msgstr ""

#: api/admin.py:100
msgid "IPv4 diff"
msgstr "IPv4 rozdíl"

#: api/admin.py:106
msgid "IPv6 diff"
msgstr "IPv6 rozdíl"

#: api/admin.py:112
msgid "Download diff"
msgstr "Rozdíl down"

#: api/admin.py:118
msgid "Upload diff"
msgstr "Rozdíl up"

#: api/models.py:24
msgid "IPv4 address"
msgstr "IPv4 adresa"

#: api/models.py:26
msgid "IPv4 RIPE netname"
msgstr ""

#: api/models.py:27
msgid "IPv4 RIPE inetnum link"
msgstr ""

#: api/models.py:28
msgid "IPv6 address"
msgstr "IPv6 adresa"

#: api/models.py:30
msgid "IPv6 RIPE netname"
msgstr ""

#: api/models.py:31
msgid "IPv6 RIPE inetnum link"
msgstr ""

#: api/models.py:34
msgid "IPv4 ratio"
msgstr "IPv4 poměr"

#: api/models.py:37
msgid "IPv6 ratio"
msgstr "IPv6 poměr"

#: api/models.py:38
msgid "Download ratio"
msgstr ""

#: api/models.py:39
msgid "Upload ratio"
msgstr ""

#: api/models.py:48 api/models.py:80
msgid "IPv4"
msgstr ""

#: api/models.py:49 api/models.py:81
msgid "IPv6"
msgstr ""

#: api/models.py:50
msgid "DNSSEC RSA"
msgstr ""

#: api/models.py:51
msgid "DNSSEC ECDSA"
msgstr ""

#: api/models.py:52
msgid "FENIX"
msgstr ""

#: api/models.py:53 api/models.py:84
msgid "Time"
msgstr "Čas"

#: api/models.py:59
msgid "Connection test"
msgstr "Test připojení"

#: api/models.py:60
msgid "Connection tests"
msgstr "Testy připojení"

#: api/models.py:79
msgid "Domain"
msgstr "Doména"

#: api/models.py:82
msgid "DNSSEC"
msgstr ""

#: api/models.py:83
msgid "HTTPS"
msgstr ""

#: api/models.py:90
msgid "Website test"
msgstr "Test webu"

#: api/models.py:91
msgid "Website tests"
msgstr "Testy webů"

#~ msgid "IPv4 speed difference"
#~ msgstr "IPv4 rozdíl rychlostí"

#~ msgid "IPv6 speed difference"
#~ msgstr "IPv6 rozdíl rychlostí"
