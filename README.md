# [standardkonektivity.cz](https://www.standardkonektivity.cz)

## Features

-   Connection capability tests
    -   IPv4/6 connectivity
    -   IPv4/6 address and info from RIPE
    -   DNSSEC support (with RSA and ECDSA)
    -   Fenix network detection
    -   Eduroam network detection (from the user's domain, not the actual connection…)
-   Connection speed tests
    -   Upload, download, ping
    -   Up/down diff calculation
    -   Uses [netmetr.cz](https://www.netmetr.cz/) web-based test under the hood
    -   Separate test for IPv4/6 (if supported)
-   User's website/domain test
    -   IPv4 support
    -   IPv6 support
    -   DNSSEC
    -   HTTPS

All results are stored by the backend app upon finishing, and are available to staff through standard Django admin interface.


## Technology stack

### Backend

-   Python 3
-   Django
-   Django REST Framework
-   DNSPython (for domain checks)
-   requests
-   GUnicorn

### Frontend

-   ES6
-   React
-   Babel + Webpack
-   styled-components

## Running

### Installing Dependencies

Python:

```
pip install -r requirements.txt
```

Javascript:

```
npm install
```

### Database

Should work with any DB that's supported by Django. Tested with SQLite and PostgreSQL.

Create tables and relations based on Django models:

```
./manage.py migrate
```

### Development

Start a webpack devserver with hot code push:

```
npm run watch
```

(runs at localhost:8001 by default)

Run the Django app:

```
export DJANGO_SECRET_KEY="your-secret-key"
./manage.py runserver
```


#### tests

Run all tests (both backend and frontend):

```
./test.sh
```

Backend tests use the standard Django's test framework. React component are tested using Jest
snapshots with plugins to support styled-components etc.

Config files for various linters (eslint, stylelint, flake8) are included in the git repo.

Note: components screenshot comparison tests do not currently work with Node 9 due to [jest-image-snapshot/issues/31](https://github.com/americanexpress/jest-image-snapshot/issues/31).

### Building for production env

Create a frontend assets bundle with compiled JS files and optimized images:

```
npm run build
```

Bundles are placed to `frontend/bundles`.


Tell Django we want to use production settings:

```
export DJANGO_SETTINGS_MODULE="server.settings.deploy"
```

Collect the frontend bundles and Django admin stuff:

```
./manage.py collectstatic
```

Static files are placed to `./static/` dir by default. Point your webserver to this location to serve static files without ever reaching Django app (example nginx config below).

Create .mo files with translations:

```
./manage.py compilemessages
```

### Server configuration

Example supervisord config:

```
[program:standard-konektivity]
command=/var/www/standard-konektivity.cz/.venv/bin/gunicorn -w 3 --timeout 120 --log-syslog --log-syslog-prefix standard-konektivity --log-level info server.wsgi:application
directory=/var/www/standard-konektivity.cz/
environment=DJANGO_SETTINGS_MODULE="server.settings.deploy",DJANGO_SECRET_KEY="your-secret-key"
user=username
autostart=true
autorestart=true
redirect_stderr=true
```

Example nginx config:

```
server {
    listen 80 default_server;
    listen [::]:80 default_server;

    root /var/www/standard-konektivity.cz;

    location ^~ /static/ {
        alias /var/www/standard-konektivity.cz/static/;
    }

    location /admin/ {
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_pass http://localhost:8000/admin/;
    }

    location / {
        proxy_set_header X-Forwarded-For $remote_addr;
        proxy_pass http://localhost:8000/;
    }
}
```
