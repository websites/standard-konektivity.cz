import "babel-polyfill";
import ReactDOM from "react-dom";
import React from "react";
import settings from "../settings.json";
import Raven from "raven-js";
import theme from "./utils/colors.js";
import {injectGlobal, ThemeProvider} from "styled-components";
import App from "./components/App";

Raven.config(settings.sentryDsn).install();

injectGlobal`
    html {
        &,
        & * {
            border: 0;
            box-sizing: border-box;
            font-size: 1em;
            margin: 0;
            padding: 0;
        }
    }

    body {
        font: 12pt/1.5em sans-serif; /* stylelint-disable-line unit-whitelist */
    }

    p,
    ul,
    ol,
    table,
    dl {
        margin: 0 0 1rem;
    }

    #tb-projects-bar {
        @media print {
            display: none !important;
        }
    }
`;

class ThemedApp extends React.Component {
    render() {
        return (
            <ThemeProvider theme={theme}>
                <App settings={settings} />
            </ThemeProvider>
        );
    }
}

ReactDOM.render(
    <ThemedApp/>,
    document.getElementById("app")
);

document.addEventListener("DOMContentLoaded", function () {
    const lang = document.documentElement.lang || window.navigator.userLanguage || window.navigator.language;

    if (/^.*cs.*$/.test(lang)) {
        window.nicWidgetsBar = new nicWidgets.topBar("CS");
    } else if (/^.*en.*$/.test(lang)) {
        window.nicWidgetsBar = new nicWidgets.topBar("EN");
    } else {
        window.nicWidgetsBar = new nicWidgets.topBar("EN"); // fall back to default
    }
});
