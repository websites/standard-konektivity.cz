import "isomorphic-fetch";

module.exports = class rsa {
    getState(options) {
        return new Promise(function (resolve, reject) {
            fetch(options.url + "?_=" + Date.now(), {cache: "no-cache"})
                .then(() => {
                    resolve("fail");
                }).catch(() => {
                    resolve("ok");
                });
        });
    }
};
