import "isomorphic-fetch";

module.exports = class ipv4 {
    getState(options) {
        return new Promise(function (resolve, reject) {
            fetch(options.url).then(() => {
                resolve("ok");
            }).catch(() => {
                resolve("fail");
            });
        });
    }
};
