import "isomorphic-fetch";

module.exports = class fenix {
    getState(options) {
        return new Promise((resolve, reject) => {
            fetch(options.url.ipv4).then((response) => {
                return response.json();
            }).then((json) => {
                if (json.result > 0) {
                    resolve("ok");
                } else {
                    fetch(options.url.ipv6).then((response) => {
                        return response.json();
                    }).then((json) => {
                        if (json.result > 0) {
                            resolve("ok");
                        } else {
                            resolve("fail");
                        }
                    }).catch(() => {
                        resolve("fail");
                    });       
                }
            }).catch(() => {
                resolve("off");
            });
        });
    }
};
