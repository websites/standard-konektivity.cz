import "isomorphic-fetch";

module.exports = class eduroam {
    getState(options) {
        return new Promise((resolve, reject) => {
            fetch(options.url + options.websiteUrl).then((response) => {
                return response.json();
            }).then((json) => {
                if (json.eduroam === true) {
                    const details = json.orgs.map((org, index) => `**Organizace: *${typeof org["org_name"] === "string" ? org["org_name"] : "—" }***\n\nRealmy${index === 0 ? " (klikněte pro více informací)" : ""}:\n${org["realms"].map(realm => `- [${realm}](https://pripojovani.eduroam.cz/${realm})`).join("\n")}`).join("\n\n");
                    resolve({
                        state: "ok",
                        details
                    });
                } else {
                    resolve({
                        state: "fail",
                        details: `Pro Vaši doménu (URL [\`${options.websiteUrl}\`](https://pripojovani.eduroam.cz/${options.websiteUrl})) nebyla v databázi členů [eduroam.cz](https://eduroam.cz) nalezena žádná shoda. Pokud jste přesto do sítě eduroam připojeni, [kontaktujte](https://www.eduroam.cz/cs/kontakty) prosím sdružení [CESNET](https://www.cesnet.cz).`
                    });
                }
            }).catch(() => {
                resolve("off");
            });
        });
    }
};
