import "isomorphic-fetch";

module.exports = class ipAddress {
    getState(options) {
        return new Promise((resolve, reject) => {
            fetch(options.url.netmetrSettings, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    language: "en",
                    type: "DESKTOP",
                    name: "RMBT",
                    terms_and_conditions_accepted_version: 1,
                    uuid: null
                })
            }).catch(() => {
                resolve("–");
            })
                .then(response => response.json())
                .then(settingsJson => 
                    fetch(options.url.ip, {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json"
                        },
                        body: JSON.stringify({
                            // some weird magic to make NetMetr respond,
                            // taken from it's Java source…
                            device: "browser",
                            language: "en",
                            model: "browser",
                            plattform: "browser",
                            softwareVersionCode: 20046,
                            uuid: settingsJson.settings[0].uuid
                        })
                    })
                        .then(response => response.json())
                        .then(netmetrJson => {
                            if (netmetrJson.ip) {
                                fetch(options.url.ripe + netmetrJson.ip)
                                    .then(response => {
                                        return response.json();
                                    })
                                    .then(ripeJson => {
                                        resolve(ripeJson);
                                    })
                                    .catch(() => {
                                        resolve("–");
                                    });
                            } else {
                                resolve("–");
                            }
                        })
                        .catch(() => {
                            resolve("–");
                        })
                );
        });
    }
};
