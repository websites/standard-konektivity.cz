import rsa from "../connection_tests/rsa.js";

test("DNSSEC RSA test works properly when given a broken domain", () => {
    const TestedClass = new rsa();

    TestedClass.getState({url: "https://bad-rsa.test-ipv6.nic.cz/"}).then(function (result) {
        expect(result).toBe("ok");
    });
});

test("DNSSEC RSA test works properly when given a valid domain", () => {
    const TestedClass = new rsa();

    TestedClass.getState({url: "https://www.nic.cz/"}).then(function (result) {
        expect(result).toBe("fail");
    });
});
