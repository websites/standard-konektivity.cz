import React from "react";
import renderer from "react-test-renderer";
import "jest-styled-components";
import Link from "../components/Link.jsx";

const TestedComponent = <Link
    url="https://www.nic.cz/"
    title="TITLE_TEXT"
    text="LINK_TEXT"
    newWindow={true}
/>;

test("Link is rendered correctly", () => {
    const component = renderer.create(TestedComponent);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

test("Link renders <a>", () => {
    const component = renderer.create(TestedComponent);

    const tree = component.toJSON();
    expect(tree.type).toBe("a");
});
