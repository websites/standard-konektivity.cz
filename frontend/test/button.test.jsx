import React from "react";
import renderer from "react-test-renderer";
import "jest-styled-components";
import Button from "../utils/button.js";

const TestedComponent = <Button>TEXT</Button>;

test("Button is rendered correctly", () => {
    const component = renderer.create(TestedComponent);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});

test("Button renders <button>", () => {
    const component = renderer.create(TestedComponent);

    const tree = component.toJSON();
    expect(tree.type).toBe("button");
});
