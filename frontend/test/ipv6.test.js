import ipv6 from "../connection_tests/ipv6.js";

test("IPv6 test works properly when given a IPv6-only domain", () => {
    const TestedClass = new ipv6();

    TestedClass.getState({url: "https://control-6.netmetr.cz/"}).then(function (result) {
        expect(result).toBe("ok");
    });
});

test("IPv6 test works properly when given a non-existing domain", () => {
    const TestedClass = new ipv6();

    TestedClass.getState({url: "https://nope/"}).then(function (result) {
        expect(result).toBe("fail");
    });
});
