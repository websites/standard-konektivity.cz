import React from "react";
import renderer from "react-test-renderer";
import "jest-styled-components";
import Input from "../utils/input.js";

const TestedComponent = <Input/>;

test("Input is rendered correctly", () => {
    const component = renderer.create(TestedComponent);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});


test("Input renders <input>", () => {
    const component = renderer.create(TestedComponent);

    const tree = component.toJSON();
    expect(tree.type).toBe("input");
});
