import React from "react";
import renderer from "react-test-renderer";
import "jest-styled-components";
import Loading from "../utils/loading.js";

const TestedComponent = <Loading/>;

test("Loading is rendered correctly", () => {
    const component = renderer.create(TestedComponent);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});


test("Loading renders <span>", () => {
    const component = renderer.create(TestedComponent);

    const tree = component.toJSON();
    expect(tree.type).toBe("span");
});
