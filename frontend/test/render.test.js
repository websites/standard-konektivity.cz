import HeadlessChrome from "simple-headless-chrome";
const { toMatchImageSnapshot } = require("jest-image-snapshot");

expect.extend({ toMatchImageSnapshot });

const browser = new HeadlessChrome({
    headless: true
});

it("renders the initial state correctly", async () => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10 * 1000;

    await browser.init();
    const tab = await browser.newTab();
    await tab.goTo("http://localhost:8000/");
    const screenshot = await tab.getScreenshot({
        format: "png"
    });
    const image = new Buffer(screenshot, "base64");

    expect(image).toMatchImageSnapshot();

    await browser.close();
});

it("renders the malformated URL warning correctly", async () => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10 * 1000;

    await browser.init();
    const tab = await browser.newTab();
    await tab.goTo("http://localhost:8000/");
    await tab.click("button");
    await tab.wait(500);
    const screenshot = await tab.getScreenshot({
        format: "png"
    });
    const image = new Buffer(screenshot, "base64");

    expect(image).toMatchImageSnapshot();

    await browser.close();
});

it("renders the test view correctly", async () => {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 10 * 1000;

    await browser.init();
    const tab = await browser.newTab();
    await tab.goTo("http://localhost:8000/");
    await tab.type("input", "nic.cz");
    await tab.click("button");
    await tab.wait(1000);
    const screenshot = await tab.getScreenshot({
        format: "png"
    });
    const image = new Buffer(screenshot, "base64");

    expect(image).toMatchImageSnapshot();

    await browser.close();
});
