import React from "react";
import renderer from "react-test-renderer";
import "jest-styled-components";
import AppHeader from "../components/AppHeader.jsx";

const TestedComponent = <AppHeader/>;

test("AppHeader is rendered correctly", () => {
    const component = renderer.create(TestedComponent);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});
