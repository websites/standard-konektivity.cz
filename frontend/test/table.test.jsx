import React from "react";
import renderer from "react-test-renderer";
import "jest-styled-components";
import {Table, Row, HeaderCell, BodyCell} from "../utils/table.js";

const TestedComponent = (
    <Table>
        <Row><HeaderCell/></Row>
        <Row><BodyCell/></Row>
    </Table>
);

test("Table is rendered correctly", () => {
    const component = renderer.create(TestedComponent);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});


test("Table renders <table>", () => {
    const component = renderer.create(TestedComponent);

    const tree = component.toJSON();
    expect(tree.type).toBe("table");
});

test("Table children render the correct elements", () => {
    const component = renderer.create(TestedComponent);

    const tree = component.toJSON();
    expect(tree.children[0].type).toBe("tr");
    expect(tree.children[0].children[0].type).toBe("th");
    expect(tree.children[1].type).toBe("tr");
    expect(tree.children[1].children[0].type).toBe("td");
});
