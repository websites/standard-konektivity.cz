import ipv4 from "../connection_tests/ipv4.js";

test("IPv4 test works properly when given a IPv4-only domain", () => {
    const TestedClass = new ipv4();

    TestedClass.getState({url: "https://control-4.netmetr.cz/"}).then(function (result) {
        expect(result).toBe("ok");
    });
});

test("IPv4 test works properly when given a non-existing domain", () => {
    const TestedClass = new ipv4();

    TestedClass.getState({url: "https://nope/"}).then(function (result) {
        expect(result).toBe("fail");
    });
});
