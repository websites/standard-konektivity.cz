import React from "react";
import renderer from "react-test-renderer";
import Container from "../utils/container.js";
import "jest-styled-components";

const TestedComponent = <Container><div/></Container>;

test("Container is rendered correctly", () => {
    const component = renderer.create(TestedComponent);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});


test("Container renders <div>", () => {
    const component = renderer.create(TestedComponent);

    const tree = component.toJSON();
    expect(tree.type).toBe("div");
});
