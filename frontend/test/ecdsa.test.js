import ecdsa from "../connection_tests/ecdsa.js";

test("DNSSEC ECDSA test works properly when given a broken domain", () => {
    const TestedClass = new ecdsa();

    TestedClass.getState({url: "https://bad-ecdsa.test-ipv6.nic.cz/"}).then(function (result) {
        expect(result).toBe("ok");
    });
});

test("DNSSEC ECDSA test works properly when given a valid domain", () => {
    const TestedClass = new ecdsa();

    TestedClass.getState({url: "https://www.nic.cz/"}).then(function (result) {
        expect(result).toBe("fail");
    });
});
