import React from "react";
import renderer from "react-test-renderer";
import "jest-styled-components";
import Hidden from "../utils/hidden.js";

const TestedComponent = <Hidden><div/></Hidden>;

test("Hidden is rendered correctly", () => {
    const component = renderer.create(TestedComponent);

    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});


test("Hidden renders <div>", () => {
    const component = renderer.create(TestedComponent);

    const tree = component.toJSON();
    expect(tree.type).toBe("div");
});
