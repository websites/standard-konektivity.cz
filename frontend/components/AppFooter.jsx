import React from "react";
import styled from "styled-components";
import logoEU from "../images/eu.png";
import logoMPR from "../images/mpr.png";
import logoMoqos from "../images/moqos.png";
import logoCZNIC from "../images/cznic.png";
import logoCEF from "../images/cef.png";

const Container = styled.footer `
    margin-top: 10vh;

    @media print {
        display: none;
    }
`;

const LogoCEF = styled.div `
    display: flex;
    justify-content: center;
    flex-wrap: wrap;

    img {
        height: 3em;
        margin-bottom: 2rem;
        max-width: 80%;
        object-fit: contain;
    }
`;

const LogoContainer = styled.div `
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    margin-bottom: 2rem;

    img {
        height: 3.5em;
        margin-bottom: 2rem;
        max-width: 80%;
        object-fit: contain;

        &:not(:last-child) {
            margin-right: 2rem;
        }
    }
`;

const FooterText = styled.p `
    border-bottom: 0.1em solid ${props => props.theme.borderColor};
    text-align: center;
    padding: 1rem;
    margin-bottom: 2rem;
`;

export default class AppFooter extends React.Component {
    render() {
        return (
            <Container>
                <FooterText>
                    Podporované prohlížeče: Chrome/Chromium&nbsp;40+,
                    Firefox&nbsp;45+, IE&nbsp;11, Edge&nbsp;13+
                </FooterText>
                <LogoContainer>
                    <img src={logoMPR} alt="logo MPR" />
                    <img src={logoMoqos} alt="logo MoQos" />
                    <img src={logoEU} alt="logo EU" />
                    <img src={logoCZNIC} alt="logo CZ.NIC" />
                </LogoContainer>
                <LogoCEF>
                    <img src={logoCEF} alt="logo CEF" />
                </LogoCEF>
            </Container>
        );
    };
}
