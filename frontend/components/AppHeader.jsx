import React from "react";
import styled from "styled-components";
import color from "color";

const Container = styled.header `
    border-bottom: 0.1em solid ${props => props.theme.borderColor};
    color: ${props => props.theme.borderColor};
    font-weight: bolder;
    padding: 1rem 10vw;
    user-select: none;
    font-size: 1.5em;

    @media (${props => props.theme.breakpoint}) {
        padding: 1rem;
    }

    @media print {
        font-size: 1em;
        padding: 0 1rem 0.25rem;
    }
`;

export default class AppHeader extends React.Component {
    render() {
        return (
            <Container>
                <h1>{this.props.title}</h1>
            </Container>
        );
    };
}
