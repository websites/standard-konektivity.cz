import React from "react";
import styled from "styled-components";

const StyledLink = styled.a `
    color: ${props => props.theme.linkColor};
    cursor: pointer;
    text-decoration: underline;
    white-space: nowrap;

    &:hover {
        text-decoration: underline;
    }

    &.netmetr {
        @media print {
            color: transparent;

            &::after {
                color: ${props => props.theme.linkColor};
                display: inline-block;
                content: attr(href);
                font-size: 0.8em;
            }
        }
    }
`;

export default class Link extends React.Component {
    render() {
        return (
            <StyledLink
                className={this.props.url.indexOf("netmetr") > -1 ? "netmetr" : null}
                href={this.props.url}
                title={this.props.title}
                target={this.props.newWindow ? "_blank" : null}
            >{this.props.text}</StyledLink>
        );
    };
}

