import React from "react";
import "whatwg-fetch";
import Link from "./Link.jsx";
import { Table, Row, HeaderCell, BodyCell } from "../utils/table.js";
import Container from "../utils/container.js";

export default class ConnectionTestResultList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
    }

    loadResultsFromServer() {
        const app = this;
        fetch(this.props.apiUrl + "connection/")
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                app.setState({ data });
            });
    }

    componentDidMount() {
        this.loadResultsFromServer();
    }

    render() {
        let resultNodes;
        const i18n = this.props.i18n;
        if (this.state.data) {
            const netmetrResultPage = this.props.netmetrResultPage;
            resultNodes = this.state.data.map(function (result) {
                let netmetrLink4, netmetrLink6;
                if (result.netmetr_uuid_ipv4) {
                    netmetrLink4 = (
                        <Link
                            text={i18n.netmetrLink}
                            newWindow={true}
                            url={`${netmetrResultPage}?${
                                result.netmetr_uuid_ipv4
                            }`}
                        />
                    );
                }
                if (result.netmetr_uuid_ipv6) {
                    netmetrLink6 = (
                        <Link
                            text={i18n.netmetrLink}
                            newWindow={true}
                            url={`${netmetrResultPage}?${
                                result.netmetr_uuid_ipv6
                            }`}
                        />
                    );
                }
                return (
                    <Row key={result.id}>
                        <BodyCell>
                            {new Date(result.timestamp).toLocaleString()}
                        </BodyCell>
                        <BodyCell>{result.user_ip}</BodyCell>
                        <BodyCell>{result.has_dnssec_rsa ? "✓" : "✗"}</BodyCell>
                        <BodyCell>
                            {result.has_dnssec_ecdsa ? "✓" : "✗"}
                        </BodyCell>
                        <BodyCell>{result.has_fenix ? "✓" : "✗"}</BodyCell>
                        <BodyCell>{result.has_ipv6 ? "✓" : "✗"}</BodyCell>
                        <BodyCell>{netmetrLink4}</BodyCell>
                        <BodyCell>{netmetrLink6}</BodyCell>
                        <BodyCell>
                            {result.speed_ipv4_down
                                ? Math.round(result.speed_ipv4_down * 100) /
                                      100 +
                                  " Mb/s"
                                : ""}
                        </BodyCell>
                        <BodyCell>
                            {result.speed_ipv4_up
                                ? Math.round(result.speed_ipv4_up * 100) / 100 +
                                  " Mb/s"
                                : ""}
                        </BodyCell>
                        <BodyCell>
                            {result.speed_ipv4_ratio
                                ? Math.round(result.speed_ipv4_ratio * 10000) /
                                      100 +
                                  " %"
                                : ""}
                        </BodyCell>
                        <BodyCell>
                            {result.ping_ipv4
                                ? Math.round(result.ping_ipv4 * 100) / 100 +
                                  " ms"
                                : ""}
                        </BodyCell>
                        <BodyCell>
                            {result.speed_ipv6_down
                                ? Math.round(result.speed_ipv6_down * 100) /
                                      100 +
                                  " Mb/s"
                                : ""}
                        </BodyCell>
                        <BodyCell>
                            {result.speed_ipv6_up
                                ? Math.round(result.speed_ipv6_up * 100) / 100 +
                                  " Mb/s"
                                : ""}
                        </BodyCell>
                        <BodyCell>
                            {result.speed_ipv6_ratio
                                ? Math.round(result.speed_ipv6_ratio * 10000) /
                                      100 +
                                  " %"
                                : ""}
                        </BodyCell>
                        <BodyCell>
                            {result.ping_ipv6
                                ? Math.round(result.ping_ipv6 * 100) / 100 +
                                  " ms"
                                : ""}
                        </BodyCell>
                    </Row>
                );
            });
        }
        return (
            <Container>
                <Table>
                    <thead>
                        <Row>
                            <HeaderCell>{this.props.i18n.timestamp}</HeaderCell>
                            <HeaderCell>{this.props.i18n.ip}</HeaderCell>
                            <HeaderCell>{this.props.i18n.rsa}</HeaderCell>
                            <HeaderCell>{this.props.i18n.ecdsa}</HeaderCell>
                            <HeaderCell>{this.props.i18n.fenix}</HeaderCell>
                            <HeaderCell>{this.props.i18n.ipv6}</HeaderCell>
                            <HeaderCell>{this.props.i18n.netmetr4}</HeaderCell>
                            <HeaderCell>{this.props.i18n.netmetr6}</HeaderCell>
                            <HeaderCell>{this.props.i18n.download4}</HeaderCell>
                            <HeaderCell>{this.props.i18n.upload4}</HeaderCell>
                            <HeaderCell>{this.props.i18n.downup4}</HeaderCell>
                            <HeaderCell>{this.props.i18n.ping4}</HeaderCell>
                            <HeaderCell>{this.props.i18n.download6}</HeaderCell>
                            <HeaderCell>{this.props.i18n.upload6}</HeaderCell>
                            <HeaderCell>{this.props.i18n.downup6}</HeaderCell>
                            <HeaderCell>{this.props.i18n.ping6}</HeaderCell>
                        </Row>
                    </thead>
                    <tbody>{resultNodes}</tbody>
                </Table>
            </Container>
        );
    }
}
