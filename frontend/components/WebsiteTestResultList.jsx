import React from "react";
import "whatwg-fetch";
import Link from "./Link.jsx";
import { Table, Row, HeaderCell, BodyCell } from "../utils/table.js";
import Container from "../utils/container.js";

export default class WebsiteTestResultList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        };
    }

    loadResultsFromServer() {
        const component = this;
        fetch(this.props.apiUrl + "website/")
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                component.setState({ data });
            });
    }

    componentDidMount() {
        this.loadResultsFromServer();
    }

    render() {
        let resultNodes;
        if (this.state.data) {
            resultNodes = this.state.data.map(function (result) {
                return (
                    <Row key={result.id}>
                        <BodyCell>
                            {new Date(result.timestamp).toLocaleString()}
                        </BodyCell>
                        <BodyCell>{result.domain}</BodyCell>
                        <BodyCell>{result.has_dnssec ? "✓" : "✗"}</BodyCell>
                        <BodyCell>{result.has_ipv6 ? "✓" : "✗"}</BodyCell>
                        <BodyCell>{result.has_https ? "✓" : "✗"}</BodyCell>
                    </Row>
                );
            });
        }
        return (
            <Container>
                <Table>
                    <thead>
                        <Row>
                            <HeaderCell>{this.props.i18n.timestamp}</HeaderCell>
                            <HeaderCell>{this.props.i18n.domain}</HeaderCell>
                            <HeaderCell>{this.props.i18n.dnssec}</HeaderCell>
                            <HeaderCell>{this.props.i18n.ipv6}</HeaderCell>
                            <HeaderCell>{this.props.i18n.https}</HeaderCell>
                        </Row>
                    </thead>
                    <tbody>{resultNodes}</tbody>
                </Table>
            </Container>
        );
    }
}
