import React from "react";
import styled from "styled-components";

const StyledHeading = styled.h2 `
    color: ${props => props.theme.buttonBg};
    white-space: nowrap;
    margin-top: 2rem;

    @media print {
        margin-top: 0.5rem;
    }
`;

export default class TestTitle extends React.Component {
    render() {
        return (
            <StyledHeading>{this.props.text}</StyledHeading>
        );
    };
}
