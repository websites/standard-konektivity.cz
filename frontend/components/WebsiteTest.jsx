import React from "react";
import styled from "styled-components";
import Button from "../utils/button.js";
import Input from "../utils/input.js";
import Container from "../utils/container.js";
import TestResultBlock from "./TestResultBlock.jsx";
import Loading from "../utils/loading.js";
import TestTitle from "./TestTitle.jsx";
import "whatwg-fetch";

export default class WebsiteTest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            testsRunning: true,
            testsFinished: false,
            saving: false,
            saved: false
        };
    }

    getTestResult() {
        const component = this;
        fetch(
            this.props.settings.urls.website + "?url=" + this.props.websiteUrl
        )
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                if (data.hasOwnProperty("error")) {
                    component.setState({
                        message: component.props.settings.i18n.badUrl,
                        testsRunning: false,
                        testsFinished: true
                    });
                } else {
                    component.setState({
                        results: data,
                        testsRunning: false,
                        testsFinished: true
                    });
                    component.saveResult();
                }
            });
    }

    saveResult() {
        const component = this;
        if (
            !this.state.saving &&
            !this.state.saved &&
            this.state.testsFinished
        ) {
            component.setState({
                saving: true
            });

            const payload = {
                has_ipv4: this.state.results.ipv4,
                has_ipv6: this.state.results.ipv6,
                has_dnssec: this.state.results.dnssec,
                has_https: this.state.results.https,
                domain: this.state.results.domain
            };

            fetch(this.props.settings.urls.api + "website/", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(payload)
            })
                .then(response => {
                    return response.json();
                })
                .then(json => {
                    component.props.storeTest("websiteTest", json);
                    component.setState({
                        saving: false,
                        saved: true,
                        testId: json.id
                    });
                })
                .catch(() => {
                    component.setState({
                        saving: false,
                        saved: false
                    });
                });
        }
    }

    componentWillMount() {
        this.getTestResult();
    }

    formatValue(value) {
        if (typeof value === "boolean") {
            return value ? "✓" : "✗";
        } else {
            return value;
        }
    }

    render() {
        let content = null;

        if (this.state.hasOwnProperty("message")) {
            content = (
                <div>
                    <TestResultBlock name={this.state.message} />
                </div>
            );
        } else {
            content = (
                <div>
                    {this.props.settings.websiteTests.map(testName => {
                        return (
                            <TestResultBlock
                                result={
                                    this.state.results &&
                                    this.state.results.hasOwnProperty(
                                        testName
                                    ) ? (
                                            this.formatValue(
                                                this.state.results[testName]
                                            )
                                        ) : (
                                            <Loading />
                                        )
                                }
                                name={
                                    this.props.settings.i18n.testLabels.website[
                                        testName
                                    ]
                                }
                                good={
                                    this.state.results &&
                                    typeof this.state.results[testName] ===
                                        "boolean"
                                        ? this.state.results[testName]
                                        : null
                                }
                                key={testName}
                            />
                        );
                    })}
                </div>
            );
        }

        return (
            <div>
                <TestTitle
                    text={this.props.settings.i18n.testTitles["website"]}
                />
                {content}
            </div>
        );
    }
}
