import React from "react";
import "whatwg-fetch";
import AppHeader from "./AppHeader.jsx";
import AppFooter from "./AppFooter.jsx";
import styled from "styled-components";
import Test from "./Test.jsx";
import Markdown from "markdown-to-jsx";

const AppContainer = styled.div`
    background: ${props => props.theme.baseBg};
    color: ${props => props.theme.baseText};
    display: flex;
    flex-direction: column;
    min-height: calc(100vh - 36px); /* stylelint-disable-line unit-whitelist */
`;

const AppContent = styled.div`
    display: flex;
    flex-direction: column;
    padding: 1rem 0;
    flex: 1 0 auto;

    @media print {
        padding: 0;
    }
`;

const TextContainer = styled.div`
    padding: 1rem 10vw;

    a {
        color: ${props => props.theme.linkColor};
        cursor: pointer;
        text-decoration: underline;
        white-space: nowrap;

        &:hover {
            text-decoration: underline;
        }
    }

    @media (${props => props.theme.breakpoint}) {
        padding: 1rem;
    }

    @media print {
        display: none;
    }
`;

export default class App extends React.Component {
    render() {
        return (
            <AppContainer>
                <AppHeader title={this.props.settings.i18n.title} />
                <AppContent>
                    <TextContainer>
                        <Markdown>{this.props.settings.i18n.intro.join("\n\n")}</Markdown>
                    </TextContainer>
                    <Test settings={this.props.settings} />
                </AppContent>
                <AppFooter />
            </AppContainer>
        );
    }
}
