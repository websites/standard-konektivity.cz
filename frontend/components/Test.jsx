import React from "react";
import ConnectionTest from "./ConnectionTest.jsx";
import WebsiteTest from "./WebsiteTest.jsx";
import TestTitle from "./TestTitle.jsx";
import TestResultBlock from "./TestResultBlock.jsx";
import Loading from "../utils/loading.js";
import Button from "../utils/button.js";
import Input from "../utils/input.js";
import Container from "../utils/container.js";
import styled, { keyframes } from "styled-components";

const anim = keyframes`
    0% {
        opacity: 0;
    }

    100% {
        opacity: 1;
    }
`;

const TestContainer = styled.div`
    display: flex;
    flex-direction: column;
    padding: 1rem 10vw;
    animation: ${anim} 0.33s 1 ease-out;

    @media (${props => props.theme.breakpoint}) {
        padding: 1rem;
    }

    @media print {
        padding: 0 1rem;
    }
`;

const ButtonContainer = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 1rem;

    @media print {
        display: none;
    }
`;

const IDText = styled.span`
    font-family: monospace;

    @media print {
        margin-right: 1em;
    }
`;

const IDContainer = styled.div`
    display: flex;
    flex-direction: column;

    @media print {
        flex-direction: row;
        font-size: 0.8em;
    }
`;

const DomainWarning = styled.div`
    display: inline-block;
    text-align: center;
    color: ${props => props.theme.badResult};
    border: 0.1em solid ${props => props.theme.badResult};
    padding: 0.5em;
    margin-top: -1em;
    animation: ${anim} 0.33s 1 ease-out;

    @media (${props => props.theme.breakpoint}) {
        margin-top: 0;
    }
`;

export default class Test extends React.Component {
    constructor(props) {
        super(props);
        this.runTests = this.runTests.bind(this);
        this.updateUrl = this.updateUrl.bind(this);
        this.resetState = this.resetState.bind(this);
        this.showPrintDialog = this.showPrintDialog.bind(this);
        this.handleKeypress = this.handleKeypress.bind(this);
        this.storeTest = this.storeTest.bind(this);
        this.state = {
            testsRunning: false,
            domainWarningShown: false,
            websiteUrl: "",
            connectionTest: {},
            websiteTest: {}
        };
    }

    runTests() {
        const urlRegex = /^(?:https?:\/\/)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)$/;
        if (this.state.websiteUrl && urlRegex.test(this.state.websiteUrl)) {
            this.setState({
                testsRunning: true,
                domainWarningShown: false
            });
        } else {
            this.setState({
                domainWarningShown: true
            });
        }
    }

    updateUrl(event) {
        this.setState({
            websiteUrl: event.target.value.toLowerCase(),
            domainWarningShown: false
        });
    }

    handleKeypress(event) {
        if (event.key === "Enter") {
            this.runTests();
        }
    }

    resetState() {
        this.setState({
            testsRunning: false,
            domainWarningShown: false,
            connectionTest: {},
            websiteTest: {}
        });
    }

    storeTest(test, result) {
        this.setState({
            [test]: result
        });
    }

    showPrintDialog() {
        window.print();
    }

    render() {
        let content;
        let domainWarning;

        if (this.state.domainWarningShown) {
            domainWarning = (
                <DomainWarning>Zadejte prosím URL nebo doménu.</DomainWarning>
            );
        }

        if (!this.state.testsRunning) {
            content = (
                <div>
                    <Container>
                        <Input
                            ref={"websiteUrl"}
                            placeholder={
                                this.props.settings.i18n.inputPlaceholder
                            }
                            aria-label={
                                this.props.settings.i18n.inputPlaceholder
                            }
                            onChange={this.updateUrl}
                            onKeyPress={this.handleKeypress}
                            value={this.state.websiteUrl}
                        />
                        <Button onClick={this.runTests}>
                            {this.props.settings.i18n.runTests}
                        </Button>
                    </Container>
                    <Container>{domainWarning}</Container>
                </div>
            );
        }

        if (this.state.testsRunning) {
            content = (
                <TestContainer>
                    <ConnectionTest
                        ref="ConnectionTest"
                        settings={this.props.settings}
                        websiteUrl={this.state.websiteUrl}
                        storeTest={this.storeTest}
                    />
                    <WebsiteTest
                        ref="WebsiteTest"
                        settings={this.props.settings}
                        websiteUrl={this.state.websiteUrl}
                        storeTest={this.storeTest}
                    />
                    <TestTitle text={this.props.settings.i18n.savedResult} />
                    <TestResultBlock
                        name={this.props.settings.i18n.connectionTestId}
                        result={
                            this.state.connectionTest.id ? (
                                <IDContainer>
                                    <IDText>
                                        {this.state.connectionTest.id}
                                    </IDText>
                                    <span>
                                        {new Date(
                                            this.state.connectionTest.timestamp
                                        ).toLocaleString()}
                                    </span>
                                </IDContainer>
                            ) : (
                                <Loading />
                            )
                        }
                    />
                    <TestResultBlock
                        name={this.props.settings.i18n.websiteTestId}
                        result={
                            this.state.websiteTest.id ? (
                                <IDContainer>
                                    <IDText>{this.state.websiteTest.id}</IDText>
                                    <span>
                                        {new Date(
                                            this.state.websiteTest.timestamp
                                        ).toLocaleString()}
                                    </span>
                                </IDContainer>
                            ) : (
                                <Loading />
                            )
                        }
                    />
                    <ButtonContainer>
                        <Button onClick={this.showPrintDialog}>
                            {this.props.settings.i18n.printDialog}
                        </Button>
                        <Button onClick={this.resetState}>
                            {this.props.settings.i18n.resetState}
                        </Button>
                    </ButtonContainer>
                </TestContainer>
            );
        }

        return content;
    }
}
