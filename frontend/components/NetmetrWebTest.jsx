import React from "react";
import "whatwg-fetch";
import Cookies from "universal-cookie";
import Link from "./Link.jsx";
import TestResultBlock from "./TestResultBlock.jsx";
import Hidden from "../utils/hidden.js";
import Loading from "../utils/loading.js";
import Container from "../utils/container.js";
import Button from "../utils/button.js";
import styled from "styled-components";

const Message = styled.p`
    padding: 1rem;
`;

const cookies = new Cookies();

export default class NetmetrWebTest extends React.Component {
    constructor(props) {
        super(props);
        this.runSpeedtest = this.runSpeedtest.bind(this);
        this.skipTest = this.skipTest.bind(this);
        this.state = {
            testRunning: false,
            testFailed: false,
            openTestId: null,
            testUuid: null,
            userUuid: null
        };
    }

    runSpeedtest() {
        if (!this.state.testRunning) {
            this.cleanup();
            this.userUuidPoller = setInterval(
                this.getUserUuid.bind(this),
                2000
            );
            this.testUuidPoller = setInterval(
                this.getTestUuid.bind(this),
                2000
            );
            this.openTestIdPoller = setInterval(
                this.getOpenTestId.bind(this),
                2000
            );
            this.testResultPoller = setInterval(
                this.getTestResult.bind(this),
                4000
            );
            this.failTimeout = setTimeout(
                this.displayFail.bind(this),
                this.props.settings.timeout * 1000
            );
            this.setState({
                testRunning: true,
                testFailed: false
            });
        }
    }

    getTestResult() {
        if (!this.state.result && this.state.openTestId) {
            const component = this;
            fetch(
                this.props.settings.backendUrl[this.props.protocol] +
                    "RMBTControlServer/opentests/" +
                    this.state.openTestId
            )
                .then(function (response) {
                    return response.json();
                })
                .then(function (data) {
                    if (data.test_duration) {
                        component.setState({
                            result: data,
                            testRunning: false,
                            testFailed: false,
                            testFinished: true
                        });
                        component.getSpeedDiff(
                            data.download_kbit / 1000,
                            data.upload_kbit / 1000
                        );
                        clearInterval(component.testResultPoller);
                        component.cleanup();
                        component.props.onTestFinished(
                            component.props.protocol,
                            data
                        );
                    }
                });
        }
    }

    getSpeedDiff(down, up) {
        const component = this;
        fetch(this.props.apiUrl + "speed_diff/?down=" + down + "&up=" + up)
            .then(function (response) {
                return response.json();
            })
            .then(function (data) {
                component.setState({ speedDiff: data.diff });
            });
    }

    uuidCookieToState(stateName, cookieName, clearPoller) {
        if (!this.state[stateName]) {
            const uuidCookie = cookies.get(cookieName, {
                domain: this.props.settings.cookieDomain
            });
            if (
                /^[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}$/.test(
                    uuidCookie
                )
            ) {
                this.setState({
                    [stateName]: uuidCookie
                });
                if (clearPoller) {
                    clearInterval(clearPoller);
                }
            }
        }
    }

    getOpenTestId() {
        const component = this;
        if (!this.state["openTestId"] && this.state.testUuid) {
            fetch(
                this.props.settings.backendUrl[this.props.protocol] +
                    "RMBTControlServer/testresultdetail",
                {
                    method: "POST",
                    headers: {
                        "Content-Type": "application/json"
                    },
                    body: JSON.stringify({
                        test_uuid: component.state.testUuid
                    })
                }
            )
                .then(function (response) {
                    return response.json();
                })
                .then(function (data) {
                    if (data.testresultdetail.length > 0) {
                        data.testresultdetail.forEach(function (item) {
                            if (item.title === "Open Test ID") {
                                component.setState({ openTestId: item.value });
                                clearInterval(component.openTestIdPoller);
                            }
                        });
                    }
                });
        }
    }

    getUserUuid() {
        this.uuidCookieToState(
            "userUuid",
            this.props.settings.cookieName,
            this.userUuidPoller
        );
    }

    getTestUuid() {
        this.uuidCookieToState("testUuid", "testUuid", this.testUuidPoller);
    }

    formatSpeed(speed) {
        if (speed === null || isNaN(speed)) {
            return "";
        }
        let formatted, unit;
        if (speed >= 1000) {
            formatted = (Math.round(speed / 1000 * 100) / 100).toString();
            unit = "Mb/s";
        } else {
            formatted = (Math.round(speed * 100) / 100).toString();
            unit = "Kb/s";
        }
        if (formatted.length < 5) {
            formatted = formatted.replace(/([,\.][0-9]+)$/, "$10");
        }
        return formatted + " " + unit;
    }

    formatPing(ms) {
        if (ms === null || isNaN(ms)) {
            return "";
        }
        let formatted, unit;
        if (ms >= 1000) {
            formatted = (Math.round(ms / 1000 * 100) / 100).toString();
            unit = "s";
        } else {
            formatted = (Math.round(ms * 100) / 100).toString();
            unit = "ms";
        }
        if (formatted.length < 5) {
            formatted = formatted.replace(/([,\.][0-9]+)$/, "$10");
        }
        return formatted + " " + unit;
    }

    displayFail() {
        if (!this.state.result) {
            this.setState({
                testFailed: true,
                testRunning: false
            });
            this.cleanup();
        }
    }

    skipTest() {
        this.props.onTestFinished(this.props.protocol, null);
    }

    cleanup() {
        this.setState({
            openTestId: null,
            testUuid: null,
            userUuid: null
        });
        clearInterval(this.userUuidPoller);
        clearInterval(this.testUuidPoller);
        clearInterval(this.openTestIdPoller);
        clearInterval(this.testResultPoller);
        clearTimeout(this.failTimeout);
        cookies.set("testUuid", {}, { expires: new Date(0) });
    }

    componentWillUnmount() {
        this.cleanup();
    }

    isRatioGood(ratio) {
        if (ratio > this.props.settings.goodSpeedRatio) return false;
        return true;
    }

    render() {
        let iframe = null;
        let resultLink = <Loading />;
        let downloadSpeed = <Loading />;
        let uploadSpeed = <Loading />;
        let pingMs = <Loading />;
        let speedDiff = <Loading />;

        if (
            this.state.testRunning === true &&
            this.state.testFailed === false
        ) {
            iframe = (
                <iframe
                    ref={"iframe"}
                    src={
                        this.props.settings.testUrl + "&" + this.props.protocol
                    }
                />
            );
        }

        if (this.state.result) {
            resultLink = (
                <Link
                    newWindow={true}
                    text={"Odkaz"}
                    url={
                        this.props.settings.resultUrl +
                        "?" +
                        this.state.result.open_test_uuid
                    }
                />
            );
            downloadSpeed = this.formatSpeed(this.state.result.download_kbit);
            uploadSpeed = this.formatSpeed(this.state.result.upload_kbit);
            pingMs = this.formatPing(this.state.result.ping_ms);
            speedDiff = this.state.speedDiff
                ? Math.round(this.state.speedDiff * 10000) / 100 + " %"
                : "";
        }

        let content;

        if (
            this.state.testRunning === true ||
            this.state.testFinished === true
        ) {
            content = (
                <div>
                    <TestResultBlock
                        result={downloadSpeed}
                        name={this.props.testLabels["download"]}
                    />
                    <TestResultBlock
                        result={uploadSpeed}
                        name={this.props.testLabels["upload"]}
                    />
                    <TestResultBlock
                        result={speedDiff}
                        good={this.isRatioGood(this.state.speedDiff)}
                        name={this.props.testLabels["diff"]}
                    />
                    <TestResultBlock
                        result={pingMs}
                        name={this.props.testLabels["ping"]}
                    />
                    <TestResultBlock
                        result={resultLink}
                        name={this.props.testLabels["link"]}
                    />
                </div>
            );
        } else if (
            this.state.testRunning === false &&
            this.state.testFailed === false &&
            !this.state.result
        ) {
            content = (
                <div>
                    <TestResultBlock
                        result={<Loading />}
                        name={this.props.labelWaiting}
                    />
                </div>
            );
        } else if (
            this.state.testRunning === false &&
            this.state.testFailed === true &&
            !this.state.result
        ) {
            content = (
                <Container>
                    <Message>{this.props.labelFailed}</Message>
                    <Container>
                        <Button onClick={this.runSpeedtest}>
                            {this.props.labelRetry}
                        </Button>
                        <Button onClick={this.skipTest}>
                            {this.props.labelSkip}
                        </Button>
                    </Container>
                </Container>
            );
        }

        return (
            <div>
                <Hidden>{iframe}</Hidden>
                {content}
            </div>
        );
    }
}
