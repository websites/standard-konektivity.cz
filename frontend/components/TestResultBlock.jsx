import React from "react";
import styled from "styled-components";
import Link from "./Link.jsx";
import DetailsIcon from "../images/info.svg";
import Markdown from "react-markdown";
import colors from "../utils/colors.js";

const StyledBox = styled.div `
    border-bottom: 0.1em solid ${props => props.theme.borderColor};
    color: ${props => props.theme.baseText};
    display: flex;
    flex-flow: row wrap;
    overflow: hidden;

    @media print {
        border-color: ${props => props.theme.lighten(props.theme.borderColor)};
    }
`;

const TestName = styled.div `
    padding: 1rem;
    flex-grow: 1;

    @media print {
        padding: 0.25rem;
    }
`;

const TestResult = styled.div `
    padding: 1rem;

    @media print {
        padding: 0.25rem;
    }
`;

const DetailsToggle = styled.button`
  border: 0;
  opacity: 0.6;
  transition: opacity 0.33s;
  width: 1.2rem;
  height: 1.2rem;
  cursor: pointer;
  background: transparent;
  margin-top: 1rem;

  &:hover,
  &:focus {
    opacity: 1;
  }

  img {
    width: 100%;
  }

  @media print {
      display: none;
  }
`;

const TestDetails = styled.div `
    padding: 0 1rem 1rem;
    width: 100%;

    p {
      margin: 0;
    }

    ul, ul {
      list-style-position: inside;
      margin-left: 0.5rem;
    }

    a {
      &,
      &:link,
      &:visited {
        color: ${colors.linkColor};
        text-decoration: none;
      }

      &:hover,
      &:focus {
        text-decoration: underline;
      }
    }

    @media print {
        display: none;
    }
`;

const Good = styled.span `
    color: ${props => props.theme.goodResult};
`;

const Bad = styled.span `
    color: ${props => props.theme.badResult};
`;

export default class TestResultBlock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showingDetails: false
        };
        this.toggleDetails = this.toggleDetails.bind(this);
    }

    toggleDetails() {
        this.setState({
            showingDetails: !this.state.showingDetails
        });
    }

    render() {
        let result = this.props.result;

        if (this.props.good === true) {
            result = (
                <Good>{this.props.result}</Good>
            );
        }

        if (this.props.good === false) {
            result = (
                <Bad>{this.props.result}</Bad>
            );
        }

        if (this.props.result.ip) {
            result = (
                <span>{this.props.result.ip} (<Link newWindow={true} text={this.props.result.netname} url={this.props.result.link} />)
                </span>
            );
        }

        return (
            <StyledBox>
                <TestName>{this.props.name}</TestName>
                {this.props.details ? <DetailsToggle onClick={this.toggleDetails}><img src={DetailsIcon} alt="?" /></DetailsToggle> : null}
                <TestResult>{result}</TestResult>
                {this.props.details && this.state.showingDetails ?
                    <TestDetails><Markdown source={this.props.details} /></TestDetails>
                    : null}
            </StyledBox>
        );
    };
}
