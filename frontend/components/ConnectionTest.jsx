import React from "react";
import styled from "styled-components";
import NetmetrWebTest from "./NetmetrWebTest.jsx";
import TestResultBlock from "./TestResultBlock.jsx";
import TestTitle from "./TestTitle.jsx";
import Button from "../utils/button.js";
import Container from "../utils/container.js";
import Loading from "../utils/loading.js";

export default class ConnectionTest extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            testsRunning: true,
            testsFinished: false,
            saving: false,
            saved: false,
            results: {},
            details: {}
        };
    }

    formatValue(value) {
        if (typeof value === "boolean") {
            return value ? "✓" : "✗";
        } else {
            if (value === "ok") return "✓";
            if (value === "fail") return "✗";
            if (value === "off") return "✗";
            return value;
        }
    }

    storeTestResult(name, result) {
        const st = this.state;
        if (typeof(result) === "string") { // "ok", "false", "off"
            st.results[name] = result;
        } else if (result.state && result.details) { // {status: "ok", details: "…"}
            st.results[name] = result.state;
            st.details[name] = result.details;
        } else if (result.ip) { // json from /api/ripe_ip_info/
            st.results[name] = result;
        }

        this.setState(st);

        if (name === "ipv4" && result != "ok") {
            this.storeNetmetrResult("ipv4", null);
        }

        if (name === "ipv6" && result != "ok") {
            this.storeNetmetrResult("ipv6", null);
        }

        if (
            Object.keys(st.results).length ===
            this.props.settings.connectionTests.length
        ) {
            this.setState({
                testsRunning: false,
                testsFinished: true
            });
            this.saveResult();
        }
    }

    getTestResult(testName) {
        const component = this;
        const testClass = new (require("../connection_tests/" +
            testName +
            ".js"))();
        testClass
            .getState({
                url: component.props.settings.urls[testName],
                websiteUrl: component.props.websiteUrl
            })
            .then(function (result) {
                component.storeTestResult(testName, result);
            });
    }

    storeNetmetrResult(protocol, data) {
        this.setState({
            ["netmetr-" + protocol]: data
        });
    }

    componentDidMount() {
        const component = this;
        component.props.settings.connectionTests.forEach(function (testName) {
            component.getTestResult(testName);
        });
    }

    componentDidUpdate() {
        this.runNetmetrIfItShould();
        this.saveResult();
    }

    runNetmetrIfItShould() {
        if (this.state.testsFinished) {
            if (
                this.state.results.ipv4 === "ok" &&
                this.refs.hasOwnProperty("netmetr-ipv4") &&
                !this.state.hasOwnProperty("netmetr-ipv4")
            ) {
                this.refs["netmetr-ipv4"].runSpeedtest();
            }
            if (
                this.state.results.ipv6 === "ok" &&
                this.refs.hasOwnProperty("netmetr-ipv6") &&
                this.state.hasOwnProperty("netmetr-ipv4") &&
                !this.state.hasOwnProperty("netmetr-ipv6")
            ) {
                this.refs["netmetr-ipv6"].runSpeedtest();
            }
        }
    }

    saveResult() {
        const component = this;
        if (
            !this.state.saving &&
            !this.state.saved &&
            this.state.testsFinished &&
            this.state.hasOwnProperty("netmetr-ipv4") &&
            this.state.hasOwnProperty("netmetr-ipv6")
        ) {
            component.setState({
                saving: true
            });

            const payload = {
                has_ipv4:
                    this.state.results.ipv4 === "ok" ? true : false,
                has_ipv6:
                    this.state.results.ipv6 === "ok" ? true : false,
                has_dnssec_rsa:
                    this.state.results.rsa === "ok" ? true : false,
                has_dnssec_ecdsa:
                    this.state.results.ecdsa === "ok" ? true : false,
                has_fenix:
                    this.state.results.fenix === "ok" ? true : false,
                has_eduroam:
                    this.state.results.eduroam === "ok" ? true : false,
                user_ipv4:
                    this.state.results.ipv4Address !== "–"
                        ? this.state.results.ipv4Address.ip || null
                        : null,
                user_ipv6:
                    this.state.results.ipv6Address !== "–"
                        ? this.state.results.ipv6Address.ip || null
                        : null,
                ripe_netname_ipv4:
                    this.state.results.ipv4Address !== "–"
                        ? this.state.results.ipv4Address.netname
                        : null,
                ripe_netname_ipv6:
                    this.state.results.ipv6Address !== "–"
                        ? this.state.results.ipv6Address.netname
                        : null,
                ripe_link_ipv4:
                    this.state.results.ipv4Address !== "–"
                        ? this.state.results.ipv4Address.link
                        : null,
                ripe_link_ipv6:
                    this.state.results.ipv6Address !== "–"
                        ? this.state.results.ipv6Address.link
                        : null
            };

            if (
                this.state["netmetr-ipv4"] != null &&
                this.state["netmetr-ipv4"].open_test_uuid
            ) {
                payload["netmetr_uuid_ipv4"] = this.state[
                    "netmetr-ipv4"
                ].open_test_uuid;
                payload["speed_ipv4_down"] =
                    this.state["netmetr-ipv4"].download_kbit / 1000;
                payload["speed_ipv4_up"] =
                    this.state["netmetr-ipv4"].upload_kbit / 1000;
                payload["ping_ipv4"] = this.state["netmetr-ipv4"].ping_ms;
            }

            if (
                this.state["netmetr-ipv6"] != null &&
                this.state["netmetr-ipv6"].open_test_uuid
            ) {
                payload["netmetr_uuid_ipv6"] = this.state[
                    "netmetr-ipv6"
                ].open_test_uuid;
                payload["speed_ipv6_down"] =
                    this.state["netmetr-ipv6"].download_kbit / 1000;
                payload["speed_ipv6_up"] =
                    this.state["netmetr-ipv6"].upload_kbit / 1000;
                payload["ping_ipv6"] = this.state["netmetr-ipv6"].ping_ms;
            }

            fetch(this.props.settings.urls.api + "connection/", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify(payload)
            })
                .then(response => {
                    return response.json();
                })
                .then(json => {
                    component.props.storeTest("connectionTest", json);
                    component.setState({
                        saving: false,
                        saved: true,
                        testId: json.id
                    });
                })
                .catch(() => {
                    component.setState({
                        saving: false,
                        saved: false
                    });
                });
        }
    }

    isResultGood(result) {
        if (result === "fail" || result === "off") {
            return false;
        } else if (result === "ok") {
            return true;
        } else {
            return null;
        }
    }

    render() {
        return (
            <div>
                <TestTitle
                    text={this.props.settings.i18n.testTitles["connection"]}
                />
                {this.props.settings.connectionTests.map(testName => {
                    return (
                        <TestResultBlock
                            result={
                                this.state.results[testName] ? (
                                    this.formatValue(
                                        this.state.results[testName]
                                    )
                                ) : (
                                    <Loading />
                                )
                            }
                            details={this.state.details[testName] || null }
                            name={
                                this.props.settings.i18n.testLabels.connection[
                                    testName
                                ]
                            }
                            good={this.isResultGood(
                                this.state.results[testName]
                            )}
                            key={testName}
                        />
                    );
                })}
                {this.props.settings.netmetr.protocols.map(protocol => {
                    if (this.state["netmetr-" + protocol] !== null) {
                        return (
                            <div key={"netmetr-" + protocol}>
                                <TestTitle
                                    text={
                                        this.props.settings.i18n.testTitles[
                                            "netmetr-" + protocol
                                        ]
                                    }
                                />
                                <NetmetrWebTest
                                    settings={this.props.settings.netmetr}
                                    onTestFinished={this.storeNetmetrResult.bind(
                                        this
                                    )}
                                    ref={"netmetr-" + protocol}
                                    apiUrl={this.props.settings.urls.api}
                                    testLabels={
                                        this.props.settings.i18n.testLabels
                                            .netmetr
                                    }
                                    labelFailed={
                                        this.props.settings.i18n.netmetrFailed
                                    }
                                    labelRetry={
                                        this.props.settings.i18n.netmetrRetry
                                    }
                                    labelSkip={
                                        this.props.settings.i18n.netmetrSkip
                                    }
                                    labelWaiting={
                                        this.props.settings.i18n.netmetrWaiting
                                    }
                                    protocol={protocol}
                                />
                            </div>
                        );
                    }
                })}
            </div>
        );
    }
}
