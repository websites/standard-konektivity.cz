import styled from "styled-components";

module.exports = styled.button `
    background: transparent;
    border-radius: 0.2em;
    border: 0.1em solid ${props => props.theme.buttonBg};
    color: ${props => props.theme.buttonBg};
    cursor: pointer;
    font-size: 1em;
    padding: 1rem;
    user-select: none;
    white-space: nowrap;

    &:not(:last-child) {
        margin-right: 1rem;
    }

    &:hover {
        background: ${props => props.theme.buttonBg};
        color: ${props => props.theme.buttonText};
    }
`;
