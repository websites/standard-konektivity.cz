import styled, {keyframes} from "styled-components";

const anim = keyframes `
    0%,
    80%,
    100% {
        box-shadow: 0 2.5em 0 -1.3em;
    }

    40% {
        box-shadow: 0 2.5em 0 0;
    }
`;

module.exports = styled.span `
    display: inline-block;
    position: absolute;
    color: ${props => props.theme.borderColor};
    margin: -2em auto 0 -2em;
    transform: translateZ(0);
    animation-delay: -0.16s;

    &,
    &::before,
    &::after {
        border-radius: 50%;
        width: 0.5em;
        height: 0.5em;
        animation-fill-mode: both;
        animation-name: ${anim};
        animation-duration: 1.8s;
        animation-iteration-count: infinite;
        animation-timing-function: ease-in-out;
    }

    &::before,
    &::after {
        content: "";
        position: absolute;
        top: 0;
    }

    &::before {
        left: -0.75em;
        animation-delay: -0.32s;
    }

    &::after {
        left: 0.75em;
    }

    @media print {
        display: none;
    }
`;
