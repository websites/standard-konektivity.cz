import color from "color";
import theme from "../../colors.json";

export default Object.assign(theme, {
    darken: (colorDef) => color(colorDef).darken(0.5).string(),
    lighten: (colorDef) => color(colorDef).lighten(0.5).string(),
    breakpoint: "screen and (max-width: 75em)"
});
