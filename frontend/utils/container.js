import styled from "styled-components";

export default styled.div `
    padding: 0 10vw 1rem;
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    align-items: center;

    @media (${ props => props.theme.breakpoint }) {
        padding: 0 1rem 1rem;
    }
`;
