import styled from "styled-components";

module.exports = styled.input `
    background: ${props => props.theme.baseBg};
    padding: 1rem;
    margin: 1rem;
    border: 0.1em solid ${props => props.theme.borderColor};

    &::placeholder {
        color: ${props => props.theme.borderColor};
    }
`;
