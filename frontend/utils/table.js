import styled from "styled-components";

const Table = styled.table `
    border-collapse: collapse;
    width: 100%;
    border: 0.1em solid ${props => props.theme.borderColor};
`;

const Row = styled.tr `
    border: 0.1em solid ${props => props.theme.borderColor};
`;

const HeaderCell = styled.th `
    padding: 0.5rem;
    font-weight: bold;
    border: 0.1em solid ${props => props.theme.borderColor};
`;

const BodyCell = styled.td `
    padding: 0.5rem;
    border: 0.1em solid ${props => props.theme.borderColor};
    white-space: nowrap;
`;

export {
    Table,
    Row,
    HeaderCell,
    BodyCell
};
