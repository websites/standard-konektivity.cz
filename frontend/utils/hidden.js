import styled from "styled-components";

module.exports = styled.div `
    height: 0;
    width: 0;
    overflow: hidden;
    visibility: hidden;
`;
