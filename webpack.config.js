const path = require("path");
const webpack = require("webpack");
const BundleTracker = require("webpack-bundle-tracker");
const CopyWebpackPlugin = require("copy-webpack-plugin");

function getPlugins() {
    const plugins = [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.NamedModulesPlugin(),
        new webpack.DefinePlugin({
            "process.env": {
                "NODE_ENV": JSON.stringify(process.env.NODE_ENV)
            }
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: "vendor",
            filename: "vendor.[hash].js",
            minChunks: (module) => module.context
                                   && module.context.indexOf("node_modules") >= 0
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: "react",
            filename: "react.[hash].js",
            minChunks: (module) => module.context
                                   && module.context.indexOf("node_modules") >= 0
                                   && module.context.indexOf("react") >= 0
        }),
        new CopyWebpackPlugin([
            {from: "./frontend/images/favicon.ico"}
        ])
    ];

    if (process.env.NODE_ENV === "production") {
        plugins.push(new BundleTracker({filename: "./server/webpack-stats-production.json"}));
    } else {
        plugins.push(new BundleTracker({filename: "./server/webpack-stats.json"}));
        plugins.push(new webpack.HotModuleReplacementPlugin());
    }

    return plugins;
}

function getEntries() {
    return process.env.NODE_ENV === "production"
        ? [ "./frontend/index" ]
        : [ "webpack-dev-server/client?http://localhost:8001",
            "webpack/hot/only-dev-server",
            "./frontend/index" ];
}

function getPublicPath() {
    return process.env.NODE_ENV === "production"
        ? "/standard/bundles/"
        : "http://localhost:8001/frontend/bundles/";
}

module.exports = {
    context: __dirname,
    entry: getEntries(),
    output: {
        path: path.resolve("./frontend/bundles/"),
        filename: "[name].[hash].js",
        publicPath: getPublicPath()
    },

    plugins: getPlugins(),

    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bundles)/,
                use: [
                    {
                        loader: "babel-loader"
                    }
                ]
            }, {
                test: /\.(gif|png|jpe?g|svg)$/i,
                loaders: [
                    "file-loader?hash=sha512&digest=hex&name=[name].[hash].[ext]", {
                        loader: "image-webpack-loader",
                        query: {
                            mozjpeg: {
                                quality: 95,
                                progressive: true
                            },
                            gifsicle: {
                                interlaced: false
                            },
                            pngquant: {
                                quality: "75-90"
                            }
                        }
                    }
                ]
            }
        ]
    },

    performance: {
        hints: process.env.NODE_ENV === "production"
            ? "warning"
            : false
    },

    devtool: "source-map",

    resolve: {
        modules: ["node_modules"],
        extensions: [".js", ".jsx"]
    },

    node: {
        fs: "empty"
    }
};
