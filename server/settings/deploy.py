from .base import *  # noqa: F403

ALLOWED_HOSTS = [
    "frontend.labs.nic.cz",
    "217.31.192.170",
    "127.0.0.1",
    "standard"
]

WEBPACK_LOADER["DEFAULT"]["STATS_FILE"] = os.path.join(BASE_DIR, "webpack-stats-production.json")
