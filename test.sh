#!/bin/sh

set -e

DJANGO_SECRET_KEY="test" .venv/bin/python3 manage.py test -v 2 &&
./node_modules/.bin/eslint . &&
./node_modules/.bin/stylelint "frontend/**/*.js" "frontend/**/*.jsx" &&
.venv/bin/flake8 .

npm run watch > /dev/null &

sleep 2

DJANGO_SECRET_KEY="test" .venv/bin/python3 manage.py runserver > /dev/null &

./node_modules/.bin/jest --verbose


kill $(jobs -p)

exit 0
